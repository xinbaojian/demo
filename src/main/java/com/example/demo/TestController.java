package com.example.demo;

import cn.dev33.satoken.annotation.SaCheckPermission;
import cn.dev33.satoken.stp.SaTokenInfo;
import cn.dev33.satoken.stp.StpUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author xinbaojian
 * @since 2024-10-16 22:21
 **/
@RestController
@RequiredArgsConstructor
@RequestMapping("/test")
public class TestController {


    @GetMapping("/index")
    @SaCheckPermission("test:index")
    public String index(){
        System.out.println(StpUtil.isLogin());
        return "Hello,World!";
    }

    @GetMapping("/login")
    public SaTokenInfo login(){
        StpUtil.login("1");
        return StpUtil.getTokenInfo();
    }
}
